<?php

    class Pago{
        //Parametros de entrada
        private $TipoPago;
        private $NCuentaCobro;
        private $Periodo;
        private $PeriodoFin;
        private $Fecha;
        private $FechaLimite;
        private $NApartamento;
        private $Propietario;
        private $DireccionEntrega;
        private $Correo;
        private $ValorParqueadero;
        private $CuartoUtil;
        private $M2;
        private $TotalPagar;
        private $IdEstado;
        private $ValorMulta;
        private $SaldoaFavor;
        private $Observaciones;
        private $NTotalPagar;
        private $Residente;

        //Definir constructor
        public function __construct(){}
        
        //Definir los métodos set y get

        public function setNCuentaCobro($NCuentaCobro)
        {
            $this->NCuentaCobro = $NCuentaCobro;
        }
        public function getNCuentaCobro()
        {
            return $this->NCuentaCobro;
        }

        public function setPeriodo($Periodo)
        {
            $this->Periodo = $Periodo;
        }
        public function getPeriodo()
        {
            return $this->Periodo;
        }


        public function setPeriodoFin($PeriodoFin)
        {
            $this->PeriodoFin = $PeriodoFin;
        }
        public function getPeriodoFin()
        {
            return $this->PeriodoFin;
        }

        public function setFecha($Fecha)
        {
            $this->Fecha = $Fecha;
        }
        public function getFecha()
        {
            return $this->Fecha;
        }

        public function setFechaLimite($FechaLimite)
        {
            $this->FechaLimite = $FechaLimite;
        }
        public function getFechaLimite()
        {
            return $this->FechaLimite;
        }

        public function setNApartamento($NApartamento)
        {
            $this->NApartamento = $NApartamento;
        }
        public function getNApartamento()
        {
            return $this->NApartamento;
        }

        public function setPropietario($Propietario)
        {
            $this->Propietario = $Propietario;
        }
        public function getPropietario()
        {
            return $this->Propietario;
        }


        public function setDireccionEntrega($DireccionEntrega)
        {
            $this->DireccionEntrega = $DireccionEntrega;
        }
        public function getDireccionEntrega()
        {
            return $this->DireccionEntrega;
        }

        public function setCorreo($Correo)
        {
            $this->Correo = $Correo;
        }
        public function getCorreo()
        {
            return $this->Correo;
        }

        public function setValorParqueadero($ValorParqueadero)
        {
            $this->ValorParqueadero = $ValorParqueadero;
        }
        public function getValorParqueadero()
        {
            return $this->ValorParqueadero;
        }

        public function setCuartoUtil($CuartoUtil)
        {
            $this->CuartoUtil = $CuartoUtil;
        }
        public function getCuartoUtil()
        {
            return $this->CuartoUtil;
        }

        public function setM2($M2)
        {
            $this->M2 = $M2;
        }
        public function getM2()
        {
            return $this->M2;
        }


        public function setTotalPagar($TotalPagar)
        {
            $this->TotalPagar = $TotalPagar;
        }
        public function getTotalPagar()
        {
            return $this->TotalPagar;
        }

        public function setTipoPago($TipoPago)
        {
            $this->TipoPago = $TipoPago;
        }
        public function getTipoPago()
        {
            return $this->TipoPago;
        }

        public function setIdEstado($IdEstado)
        {
            $this->IdEstado = $IdEstado;
        }
        public function getIdEstado()
        {
            return $this->IdEstado;
        }

        public function setValorMulta($ValorMulta)
        {
            $this->ValorMulta = $ValorMulta;
        }
        public function getValorMulta()
        {
            return $this->ValorMulta;
        }

        public function setSaldoaFavor($SaldoaFavor)
        {
            $this->SaldoaFavor = $SaldoaFavor;
        }
        public function getSaldoaFavor()
        {
            return $this->SaldoaFavor;
        }
        
        
        public function setObservaciones($Observaciones)
        {
            $this->Observaciones = $Observaciones;
        }
        public function getObservaciones()
        {
            return $this->Observaciones;
        }

        public function setNTotalPagar($NTotalPagar)
        {
            $this->NTotalPagar = $NTotalPagar;
        }
        public function getNTotalPagar()
        {
            return $this->NTotalPagar;
        }

        public function setResidente($Residente)
        {
            $this->Residente = $Residente;
        }
        public function getResidente()
        {
            return $this->Residente;
        }
    }
    

?>
<?php
require_once('../conexion.php'); 
require_once('../Modelo/CrudApartamento.php'); //Incluir el modelo crud apartamento
require_once('../Modelo/Apartamento.php');

$CrudApartamento = new CrudApartamento();
$ListasApartamentos = $CrudApartamento->ListarApartamento(); //llamado al metodo ListarApartamento
//var_dump($ListasApartamentos);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apartamentos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script> 
  <script>
    $(document).ready(function () {
   $('#entradafilter').keyup(function () {
      var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        })

});
</script>
</head>
<body>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
  <center>
   <h1 style="font-family:fantasy">APARTAMENTOS</h1>
    <div class="content">
        <div class="leftboton">
            <a href="IngresarApartamento.php" title="Agregar Apartamento"><i class="far fa-plus-square fa-3x"></i></a>
        </div>
        <div class="buscar">
            <label for="radio" class="control-label col-xs-3">Buscar:</label>
            <div class="col-xs-6">
                <input id="entradafilter" type="text" class="form-control">
            </div>
        </div>
    </div>
    <br>
   <table class="table" style="width:80%">
   <thead class="thead-dark">
   <tr>
   	<th scope="col">Número apartamento</th>
   	<th scope="col">Parqueadero</th>
    <th scope="col">Valor Cuota de administración</th>
    <th scope="col">Estado</th>
   	<th scope="col">Acciones</th>
   </tr>
   </thead>

   <tbody class="contenidobusqueda">
   	<?php
   	foreach($ListasApartamentos as $apartamento){
        $Valor = $apartamento->getValorMetrosCuadrados();
   		?>
        <tr>

   		<td align="center"><?php echo $apartamento->getNApartamento(); ?></td>
   		<td><?php echo $apartamento->getParqueadero(); ?></td>
      <td><?php echo '$' . number_format($Valor, 2); ?></td>
      <td><?php echo $apartamento->getIdEstado(); ?></td>
   	  <td>
        <a href="ConsultarApartamento.php?NApartamento=<?php echo $apartamento->getNApartamento(); ?>" title="Consultar Apartamento"><i class="fas fa-eye fa-lg"></i></a>
        <a href="EditarApartamento.php?NApartamento=<?php echo $apartamento->getNApartamento(); ?>" title="Editar Apartamento"><i class="fas fa-pen fa-lg"></i></a>
        <?php
            if ($apartamento->getIdEstado()=='Activo')
            {?>
            <a href="../Controlador/ControladorApartamento.php?NApartamento=<?php echo $apartamento->getNApartamento();?>&Accion=CambiarDeEstado" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
        else if ($apartamento->getIdEstado()=='Inactivo')
        {?>
            <a href="../Controlador/ControladorApartamento.php?NApartamento=<?php echo $apartamento->getNApartamento();?>&Accion=CambiarDeEstadoActivo" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
    }
    ?>
   	

   </tbody>
</table>
</center>
</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>
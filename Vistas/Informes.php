<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>  
    <title>Informes</title>
    <script>
    $(document).ready(function () {
   $('#entradafilter').keyup(function () {
      var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        })

});
    </script>
</head>
<body>
<div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
<center>
    <h1 style="font-family:fantasy">INFORMES</h1>
    
    <form  class="form-horizontal" action="#" method="post" style="align-content: center">
<div class="form-group" style="position:relative; left:842px;">
<label for="radio" class="control-label col-xs-1">Buscar:</label>
    <div class="col-xs-2">
        <input id="entradafilter" type="text" class="form-control">
    </div>
</div>
</form>
    
<table class="table" style="width:70%">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Cuenta</th>
      <th scope="col">Propietario</th>
      <th scope="col">Apto</th>
      <th scope="col">Tipo pago</th>
      <th scope="col">Periodo</th>
      <th scope="col">Estado</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody class="contenidobusqueda">
  <?php
        $Db = Db::Conectar();
        //$Sql = $Db->query('SELECT p.Cedula,p.Nombre,p.Telefono,p.Direccion,p.Correo,e.NombreEstado,da.NApartamento from propietarios p INNER JOIN estados e ON (p.IdEstado=e.IdEstado) INNER JOIN detallepropietarioapartamento da ON 
        //da.CedulaPropietario = p.Cedula');
        $filas_por_pagina = 10;
        
        $iniciar = ($_GET['pagina']-1)*$filas_por_pagina;
       
        //paginación
        $Sql_filas = 'SELECT pa.NCuentaCobro,pa.Propietario,t.NombreTipoPago,pa.Periodo,e.NombreEstado,pa.NApartamento from pagos pa 
        INNER JOIN estados e ON (pa.IdEstado=e.IdEstado)
        INNER JOIN tipospagos t ON (pa.TipoPago=t.IdTiposPagos) LIMIT :iniciar,:nfilas';
        $sentencia_filas = $Db->prepare($Sql_filas);
        $sentencia_filas->bindParam(':iniciar', $iniciar, PDO::PARAM_INT);
        $sentencia_filas->bindParam(':nfilas', $filas_por_pagina, PDO::PARAM_INT);
        $sentencia_filas->execute();

        //$resultado_filas = $sentencia_filas->fetchAll();
        //fin paginación
        $Sql = $Db->query('SELECT pa.NCuentaCobro,pa.Propietario,t.NombreTipoPago,pa.Periodo,e.NombreEstado,pa.NApartamento from pagos pa
        INNER JOIN estados e ON (pa.IdEstado=e.IdEstado)
        INNER JOIN tipospagos t ON (pa.TipoPago=t.IdTiposPagos)');
        $Sql->execute();

        //contar el número de filas
        $Total_filas = $Sql->rowCount();

        $paginas = $Total_filas/$filas_por_pagina;
        $paginas = ceil($paginas);
        if ($_GET['pagina']>$paginas) {
            header('Location:Informes.php?pagina=1');
        }
        
        while($row=$sentencia_filas->fetch(PDO::FETCH_ASSOC))
        {
        extract((array)$row);
        ?>
        <tr>
            <td><?php echo $row["NCuentaCobro"];?></td>
            <td><?php echo $row["Propietario"];?></td>
            <td><?php echo $row["NApartamento"];?></td>
            <td><?php echo $row["NombreTipoPago"];?></td>
            <td><?php echo $row["Periodo"];?></td>
            <td><?php echo $row["NombreEstado"];?></td>
            <td><a href="../TCPDF/examples/example_014.php?NCuentaCobro=<?php echo $row["NCuentaCobro"]; ?>" title="PDF"><i class="fas fa-file-alt fa-lg"></i></a>
            <?php
                $NCuentaCobro = $row["NCuentaCobro"];
                
                    echo '<a href="ConsultarInformes.php?pagina=1&NCuentaCobro='.$NCuentaCobro.'" title="Ver informe">
                    <i class="fas fa-eye fa-lg"></i></a>';}
                
            ?>
            </td>  
            </tr>
        
    
  </tbody>
</table>
<div>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">

    <li class="page-item <?php echo($_GET['pagina']<=1 ? 'disabled': '') ?>">
    <a class="page-link" href="Informes.php?pagina=<?php echo($_GET['pagina']-1); ?>">Anterior</a></li>

    <?php
        for ($i=0; $i < $paginas; $i++):
    ?>
    <li class="page-item <?php echo($_GET['pagina']==$i+1 ? 'active' : '') ?>">
    <a class="page-link" href="Informes.php?pagina=<?php echo($i+1); ?>"><?php echo($i+1); ?></a></li>
    <?php endfor ?>
    <li class="page-item <?php echo($_GET['pagina']>=$paginas ? 'disabled': '') ?>">
    <a class="page-link" href="Informes.php?pagina=<?php echo($_GET['pagina']+1); ?>">Siguiente</a></li>
  </ul>
</nav>
</div>
<br><br>
<footer align="center" class="border-top footer" style="font-family:fantasy">
    <div class="container">
        &copy; 2020 - S.T.A.B
    </div>
</footer>
</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>
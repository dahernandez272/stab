<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
require_once('../Modelo/CrudPago.php');
require_once('../Modelo/Pago.php');

$CrudPago = new CrudPago();
$Pago = $CrudPago::ActualizarPago($_GET["NCuentaCobro"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>  

    <title>Consultar pago adelantado</title>
</head>
<body>
<header>
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-black ">
            <div class="container">
                <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
                    <ul class="navbar-nav flex-grow-1">
                    <li class="nav-item">
                            <a href="Inicio.php" class="nav-link" style="color:black;"><i class="fa fa-home"></i> Volver al Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="Menu.php" class="nav-link" style="color:black;"><i class="far fa-money-bill-alt"></i> Pagos</a>
                        </li>
                        <li class="nav-item">
                            <a href="ListadoPropietarios.php" class="nav-link" style="color:black;"><i class="fas fa-user"></i> Propietarios</a>
                        </li>
                        <li class="nav-item">
                            <a href="../CerrarSesion.php" class="nav-link" style="color:black;"><i class="fa fa-power-off"></i> Cerrar sesión</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <center>
        <h1>CONSULTAR PAGO ADELANTADO</h1> <br>
    <form class="form-horizontal" action="../Controlador/ControladorPago.php" method="post" style="align-content: center" id="FrmPagos" name="FrmPagos">
        <div class="form-group">
        <label for="radio" class="control-label col-xs-3">Entidad:</label> 
        <div class="col-xs-2">
        <input type="text" class="form-control" id="Entidad" name="Entidad" value="Av.Villas" disabled > 
        </div>
        <label for="radio" class="control-label col-xs-3">Tipo de cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="Corriente" class="form-control" id="TipoCuenta" name="TipoCuenta" ">
        </div>
    </div>


    <div class="form-group">
        <label for="" class="control-label col-xs-3">Titular:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="C.R. Altobelo P.H" class="form-control" name="Titular" id="Titular" >
        </div>
        <label for="" class="control-label col-xs-3" id="label1">N° cuenta:</label> 
        <div class="col-xs-2">
        <input type="text" disabled value="50317248" class="form-control" name="Ncuenta" id="Ncuenta" >
    </div>
    </div>    
    
    <div class="form-group">
            <label for="" id="label2" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNcuentaCobro"></label> N° cuenta de cobro:</label> 
            <div class="col-xs-2">
            <input type="text" class="form-control" name="NcuentaCobro" readonly id="NcuentaCobro" 
            value="<?php echo $Pago->getNCuentaCobro();?>">
        </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="" id="label3"><label style="color: red;" for="" id="ValidarPeriodoInicio"></label> Periodo Inicio:</label> 
            <div class="col-xs-2">
            <input type="month" value="<?php echo $Pago->getPeriodoInicio();?>" disabled class="form-control calendarioC" name="PeriodoInicio" placeholder="MM-AAAA"  id="PeriodoInicio" onchange="validarFechas()">
            </div>
            <label  class="control-label col-xs-3" for=""><label for="" style="color: red;" id="ValidarPeriodoFin"></label> Periodo Fin:</label> 
            <div class="col-xs-2">
            <input type="month" value="<?php echo $Pago->getPeriodoFin();?>" disabled class="form-control calendarioC"  name="PeriodoFin" id="PeriodoFin" placeholder="MM-AAAA" onchange="validarFechas()">
        </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-xs-3">Fecha:</label>
            <div class="col-xs-2">
            <input type="date" value="<?php echo $Pago->getFechaActual();?>" name="FechaActual" id="FechaActual" class="form-control dateconsulta" >
            </div>  
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarFechaLimite"></label> Fecha límite:</label>
            <div class="col-xs-2">
            <input type="datetime" value="<?php echo $Pago->getFechaLimite();?>" disabled name="FechaLimite" id="FechaLimite" class="form-control datetimeconsulta">
        </div>
        </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3"><label for=""  style="color: red;"id="ValidarNapartamento"></label> N° apartamento:</label>
            <div class="col-xs-2">
            <input type="text" value="<?php echo $Pago->getNApartamento();?>" name="Napartamento" id="Napartamento" class="form-control" readonly onchange="validarinfoApto()">
            </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPropietario"></label>Propietario:</label>
            <div class="col-xs-2">
            <input type="text" value="<?php echo $Pago->getPropietario();?>" name="Propietario"  id="Propietario" readonly class="form-control">
            </div>  
            
            </div>
            <div class="form-group">
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarDireccionEntrega"></label>Dirección de entrega:</label>
            <div class="col-xs-2">
            <input type="text" value="<?php echo $Pago->getDireccionEntrega();?>" readonly name="DireccionEntrega" id="DireccionEntrega" class="form-control">
            </div>
            <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarEmail"></label>Email:</label>
            <div class="col-xs-2">
            <input type="text" value="<?php echo $Pago->getEmail();?>" id="Email" readonly name="Email" class="form-control">
            </div>
            </div>
            <br><br>
    
        
                    <label for="" id="labelDetalles">Conceptos que intervienen en la cuota de administración</label>
                    
                    <br>
                    <label for="" class="control-label col-xs-3">Concepto</label> 
                    <label for="" class="control-label col-xs-3">Valor</label> 
                    <label for="" class="control-label col-xs-3">Observaciones</label>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarParqueadero"></label>Parqueadero integral</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="Parqueadero" id="Parqueadero" readonly onchange="validarFechas()" value="<?php echo $Pago->getParqueadero();?>" style="text-align:right">
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarCuartoutil"></label>Cuarto útil</p> 
                    <div class="midiv col-xs-1">
                    <input for="" class="form-control" name="Cuarto" id="Cuarto" readonly onchange="validarFechas()" value="<?php echo $Pago->getCuartoUtil();?>" style="text-align:right"> 
                    </div>
                    <br><br>
                    <p for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarM2"></label>M2</p> 
                    <div class="midiv col-xs-1">
                    <input class="form-control" name="M2" readonly value="<?php echo $Pago->getM2();?>" onchange="validarFechas()" id="M2" style="text-align:right"> 
                    </div>
                    <br><br>
                    <label for="" class="control-label col-xs-3"><label for="" style="color: red;" id="ValidarPagoTotal"></label> Total a pagar:</label>
                    <div class="midiv col-xs-1">
                    <input type="text" id="TotalPagar" value="<?php echo $Pago->getTotalPagar();?>" readonly name="TotalPagar" class="form-control" style="text-align:right">
                    </div>
                <br><br><br>
    </form>
    <p align="center" id="RespuestaTransaccion"></p>
    <br><br><br>
    <footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - Didier Alonso Hernandez Perez
        </div>
    </footer>
        </center>
        
</body>
<script src="../js/funciones.js"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>